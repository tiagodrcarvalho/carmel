# Nvidia Jetson AGX Xavier Carmel PMU


***
## Enabling the "Nvidia Jetson AGX Xavier Carmel PMU" Component

First step is to clone this project inside the src/components/ folder of PAPI.
Then, to enable reading of Carmel counters the user needs to link against a
PAPI library that was configured with the Carmel component enabled.  As an
example the following command: `./configure --with-components="carmel"` is
sufficient to enable the component.

Typically, the utility `papi_components_avail` (available in
`papi/src/utils/papi_components_avail`) will display the components available
to the user, and whether they are disabled, and when they are disabled why.

