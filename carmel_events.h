/*
*  Copyright 2022 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/
#include <string.h>

#define UNIT_CORE 0
#define UNIT_L2 1
#define UNIT_SCF 2

#define CLUSTER_ATTRIBUTE "cluster"
#define CLUSTER_ATTRIBUTE_SIZE 7

#define PERF_TYPE_ARMV8 6
#define PERF_TYPE_CARMEL 7

#define CARMEL_TOTAL_CORE_COUNTERS 25

#define CARMEL_TOTAL_UNCORE_COUNTERS 9

#define CARMEL_MAX_TOTAL_COUNTERS CARMEL_TOTAL_CORE_COUNTERS + CARMEL_TOTAL_UNCORE_COUNTERS

/* Declare our vector in advance */
/* This allows us to modify the component info */
papi_vector_t _carmel_vector;

typedef struct carmel_counter
{
   unsigned int event;
   char name[PAPI_MAX_STR_LEN];	    /**< Name of the counter         */
   char description[PAPI_MAX_STR_LEN]; /**< Description of the counter  */
   unsigned int unit;		   /**UNIT_L2 or UNIT_SCF */
} carmel_counter_t;


carmel_counter_t available_counters[] = {
  {0x01, "L1I_CACHE_REFILL",    "Level-1 Instruction Cache refill (core)",     UNIT_CORE},
  {0x02, "L1I_TLB_REFILL",      "Level-1 instruction TLB refill (core)",       UNIT_CORE},
  {0x03, "L1D_CACHE_REFILL",    "Level-1 Data Cache refill (core)",            UNIT_CORE},
  {0x04, "L1D_CACHE",           "Level-1 Data Cache access (core)",            UNIT_CORE},
  {0x05, "L1D_TLB_REFILL",      "Level-1 data TLB refill (core)",              UNIT_CORE},
  //0x06 - 0x07 NOT SUPPORTED 
  {0x08, "INST_RETIRED",        "Instruction architecturally executed (core)", UNIT_CORE},
  {0x09, "EXC_TAKEN",           "Exception taken (core)",                      UNIT_CORE},
  {0x0a, "EXC_RETURN",          "Instruction architecturally executed, condition code check pass, exception return (core)",   UNIT_CORE},
  {0x0b, "CID_WRITE_RETIRED",   "Instruction architecturally executed condition code check pass, Write to CONTEXTIDR (core)", UNIT_CORE},
  //0x0c - 0x0f NOT SUPPORTED
  {0x10, "BR_MIS_PRED",         "Mispredicted or not predicted branch speculatively executed (core)", UNIT_CORE},
  {0x11, "CPU_CYCLES",          "Cycle (core)",                                     UNIT_CORE},
  {0x12, "BR_PRED",             "Predictable branch speculatively executed (core)", UNIT_CORE},
  {0x13, "MEM_ACCESS",          "Data memory access (core)",                        UNIT_CORE},
  {0x14, "L1I_CACHE",           "Level-1 Instruction Cache access (core)",          UNIT_CORE},
  {0x15, "L1D_CACHE_WB",        "Level-1 Data Cache Write-back (core)",             UNIT_CORE},
  //0x1b NOT SUPPORTED
  {0x1c, "TTBR_WRITE_RETIRED",  "Instruction architecturally executed, condition code check pass, Write to TTBR (core)", UNIT_CORE},
  //0x1d See Uncore events below
  //0x1e - 0x22 NOT SUPPORTED
  {0x23, "STALL_FRONTEND",      "No operation issued due to the frontend (core)", UNIT_CORE},
  {0x24, "STALL_BACKEND",       "No operation issued due to the backend (core)",  UNIT_CORE},
  //0x25 - 0x28 NOT SUPPORTED
  //0x29 - 0x2c See Uncore events
  //0x2d - 0x3f NOT SUPPORTED
  {0x40, "L1D_CACHE_LD",        "Level-1 Data Cache access, Read (core)",       UNIT_CORE},
  {0x41, "L1D_CACHE_ST",        "Level-1 Data Cache access, Write (core)",      UNIT_CORE},
  {0x42, "L1D_CACHE_REFILL_LD", "Level-1 Data Cache refill, Read (core)",       UNIT_CORE},
  {0x43, "L1D_CACHE_REFILL_ST", "Level-1 Data Cache refill, Write (core)",      UNIT_CORE},
  //0x44 - 0x45 NOT SUPPORTED 
  {0x46, "L1D_CACHE_WB_VICTIM", "Level-1 Data Cache Write-back, victim (core)", UNIT_CORE},
  //0x47 - 0xbf NOT SUPPORTED 
  {0xc0, "CPU_CYCLES_DUAL_EXEC", "Cycles in dual execution mode (core)",        UNIT_CORE},
  {0xc1, "CPU_CYCLES_DUAL_EXEC_ELIGIBLE", "Cycles dual execution mode is eligible to execute. Counts cycles spent in EL modes where dual execution is enabled. (core)",  UNIT_CORE},
  //0xc2 - 0xff NOT SUPPORTED
  {0x160, "L2D_CACHE", 		      "Level-2 Data Cache access (uncore).                               Use "CLUSTER_ATTRIBUTE"=<0-3> to manually specity a target cluster.", UNIT_L2},
  {0x170, "L2D_CACHE_REFILL", 	"Level-2 Data Cache refill (uncore).                               Use "CLUSTER_ATTRIBUTE"=<0-3> to manually specity a target cluster.", UNIT_L2},
  {0x180, "L2D_CACHE_WB", 		  "Attributable Level-2 Data Cache Write-back (uncore).              Use "CLUSTER_ATTRIBUTE"=<0-3> to manually specity a target cluster.", UNIT_L2},
  {0x190, "BUS_ACCESS", 	    	"Bus access (uncore)", 								                	 UNIT_SCF},
  {0x1d0, "BUS_CYCLES",		      "Bus cycle (uncore)", 								                 	 UNIT_SCF},
  {0x290, "L3D_CACHE_ALOCATE",  "Level-3 Data Cache allocation without refill (uncore)", UNIT_SCF},
  {0x2a0, "L3D_CACHE_REFILL", 	"Level-3 Data Cache refill (uncore)", 	          			 UNIT_SCF},
  {0x2b0, "L3D_CACHE", 		      "Level-3 Data Cache access (uncore)", 	          			 UNIT_SCF},
  {0x2c0, "L3D_CACHE_WB", 		  "Level-3 Data Cache access (uncore)", 	          			 UNIT_SCF}
};

#define FIRST_EVENT_VALUE 0x01
#define MAX_EVENT_VALUE 0x2c0


//legacy code - just a helper index for the counters
// #define L2D_CACHE  		 	  0
// #define L2D_CACHE_REFILL 	1
// #define L2D_CACHE_WB  		2
// #define BUS_ACCESS  		  3
// #define BUS_CYCLES 			  4
// #define L3D_CACHE_ALOCATE 5
// #define L3D_CACHE_REFILL  6
// #define L3D_CACHE  			  7
// #define L3D_CACHE_WB  		8
