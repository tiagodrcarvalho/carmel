/*
*  Copyright 2022 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

/**
 * @file    carmel_uncore.c
 * @author  Tiago Carvalho
 *          tdc@isep.ipp.pt
 * @author  Luís Miguel Pinho
 *          lmp@isep.ipp.pt
 *
 * @ingroup papi_components
 *
 * @brief
 *	This is a component speciffically for uncore events of Nvidia Xavier platforms.
 */
#include "carmel.h"

static carmel_native_event_entry_t *carmel_native_table;

/*************************************************************************/
/* Below is the actual "hardware implementation" of our example counters */
/*************************************************************************/


/** Open a perf event and return the file descriptor */
static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
        int ret;
        ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
                      group_fd, flags);
        return ret;
}

static unsigned int extract_event(unsigned int EventCode){
	return (EventCode & EVENT_MASK) >> NATIVE_EVENT_SHIFT;
}

static unsigned int extract_cluster(unsigned int EventCode){
	return (EventCode & CLUSTER_MASK) >> CLUSTER_VALUE_SHIFT;
}

static unsigned int is_cluster_setted(unsigned int EventCode){
	return (EventCode & CLUSTER_SETTED_MASK) >> CLUSTER_SETTED_SHIFT;
}

static int carmel_event_index_of(int event){
  for (int i = 0; i < CARMEL_MAX_TOTAL_COUNTERS; i++ )
		if ( event == carmel_native_table[i].event ) {
				return i;
		}
  return -1;
}


/********************************************************************/
/* Below are the functions required by the PAPI component interface */
/********************************************************************/


/** Initialize hardware counters, setup the function vector table
 * and get hardware information, this routine is called when the
 * PAPI process is initialized (IE PAPI_library_init)
 */
int _carmel_init_component( int cidx )
{

	SUBDBG( "_carmel_init_component..." );
   
   /* TODO - First, detect that our hardware is available */
   
//    if (detect_example()!=PAPI_OK) {
//       int strErr=snprintf(_carmel_vector.cmp_info.disabled_reason, PAPI_MAX_STR_LEN,
//          "Example Hardware not present.");
//       _carmel_vector.cmp_info.disabled_reason[PAPI_MAX_STR_LEN-1]=0;    // force null termination.
//       if (strErr > PAPI_MAX_STR_LEN) HANDLE_STRING_ERROR;
//       return PAPI_ENOSUPP;
//    }
   
	int num_events = CARMEL_MAX_TOTAL_COUNTERS;

	/* Allocate memory for the our native event table */
	carmel_native_table =
		( carmel_native_event_entry_t * )
		papi_calloc( num_events, sizeof(carmel_native_event_entry_t) );
	if ( carmel_native_table == NULL ) {
      int strErr=snprintf(_carmel_vector.cmp_info.disabled_reason, PAPI_MAX_STR_LEN,
      "Could not allocate %lu bytes of memory for carmel device structure.", num_events*sizeof(carmel_native_event_entry_t));
      _carmel_vector.cmp_info.disabled_reason[PAPI_MAX_STR_LEN-1]=0;    // force null termination.
      if (strErr > PAPI_MAX_STR_LEN) HANDLE_STRING_ERROR;
		return PAPI_ENOMEM;
	}

	/* fill in the event table parameters */
	for(int i = 0; i < num_events; i++){
		strcpy( carmel_native_table[i].name, available_counters[i].name );
		strcpy( carmel_native_table[i].description, available_counters[i].description );
		carmel_native_table[i].event = available_counters[i].event;	
		carmel_native_table[i].unit = available_counters[i].unit;	
		carmel_native_table[i].writable = 0; //not sure about this
		// if(i < num_events-1)
			// enum_next_event[available_counters[i].event] = available_counters[i+1].event;
	}

	/* Export the total number of events available */
	_carmel_vector.cmp_info.num_native_events = num_events;
	/* Export the component id */
	_carmel_vector.cmp_info.CmpIdx = cidx;
	return PAPI_OK;
}

/** This is called whenever a thread is initialized */
int _carmel_init_thread( hwd_context_t *ctx )
{

	carmel_context_t *carmel_context = (carmel_context_t *)ctx;
	/* clear the context structure and mark as initialized */
	memset( carmel_context, 0, sizeof ( carmel_context_t ) );
	carmel_context->initialized=1;

	SUBDBG( "_carmel_init_thread %p...", ctx );

	return PAPI_OK;
}



/** Setup a counter control state.
 *   In general a control state holds the hardware info for an
 *   EventSet.
 */

int _carmel_init_control_state( hwd_control_state_t * ctl )
{
   SUBDBG( "carmel_init_control_state... %p\n", ctl );

  SUBDBG("[CARMEL] Control state initialized!\n");

	 carmel_control_state_t *cu_ctl = ( carmel_control_state_t * ) ctl;
   memset( cu_ctl, 0, sizeof ( carmel_control_state_t ) );
   
	// /* Set the domain */
	_carmel_set_domain( ctl, _carmel_vector.cmp_info.default_domain );

	// /* default granularity */
	cu_ctl->granularity= _carmel_vector.cmp_info.default_granularity;

	// /* overflow signal */
	// cu_ctl->overflow_signal=_carmel_vector.cmp_info.hardware_intr_sig;

	/* Set cpu number in the control block to show events */
	/* are not tied to specific cpu                       */
	cu_ctl->cpu = -1;

   return PAPI_OK;
}



static int close_event( carmel_event_info_t *event )
{
	int close_error=0;

	if ( close( event->event_fd ) ) {
		PAPIERROR( "close of fd = %d returned error: %s",
			event->event_fd, strerror( errno ) );
		close_error=1;
	}


	if (close_error) {
		return PAPI_ESYS;
	}
	event->event_opened=0;

	return 0;
}

/* Close all of the opened events */
static int close_cu_events( carmel_context_t *ctx, carmel_control_state_t *ctl)
{
	int i,result;
	int num_closed=0;
	int num_opened=0;

	/* should this be a more serious error? */
	if ( ctx->state & EVENTS_RUNNING ) {
		SUBDBG("Closing without stopping first\n");
	}

	/* Close the group leader should suffice? still testing*/
	for( i=0; i<ctl->num_events; i++ ) {
		// if (ctl->events[i].group_leader_fd==-1) 
		{
			if (ctl->events[i].event_opened) {
				num_opened++;
				result=close_event(&ctl->events[i]);
				if (result==0)
					num_closed++;
			}
		}
	}

	if (num_opened != num_closed) {
			PAPIERROR("[CARMEL][ERROR] Didn't close all event leaders: "
				"Closed %d, Expected %d",
				num_closed,num_opened);
			return PAPI_EBUG;
	}

	ctl->num_events=0;
	ctl->num_core_events=0;
	ctl->num_uncore_events=0;
	ctl->group_leader_core=-1;
	ctl->group_leader_uncore=-1;

	ctx->state &= ~EVENTS_OPENED;

	return PAPI_OK;
}


			//always open detached to cpu
        	// fd1 = perf_event_open(&pe, -1, 0, -1, 0x8);


/* The read format on perf_event varies based on various flags that */
/* are passed into it.  This helper avoids copying this logic       */
/* multiple places.                                                 */
static unsigned int
get_read_format( unsigned int multiplex,
		 unsigned int inherit,
		 int format_group )
{
   unsigned int format = 0;

   /* if we need read format options for multiplexing, add them now */
   if (multiplex) {
      format |= PERF_FORMAT_TOTAL_TIME_ENABLED;
      format |= PERF_FORMAT_TOTAL_TIME_RUNNING;
   }

   /* if our kernel supports it and we are not using inherit, */
   /* add the group read options                              */
   if (!inherit) {
      if (format_group) {
	 	format |= PERF_FORMAT_GROUP;
      }
   }

   SUBDBG("multiplex: %d, inherit: %d, group_leader: %d, format: %#x\n",
	  multiplex, inherit, format_group, format);

   return format;
}


static int map_perf_event_errors_to_papi(int perf_event_error) {

   int ret;

   /* These mappings are approximate.
      EINVAL in particular can mean lots of different things */
   switch(perf_event_error) {
      case EPERM:
      case EACCES:
           ret = PAPI_EPERM;
	   break;
      case ENODEV:
      case EOPNOTSUPP:
	   ret = PAPI_ENOSUPP;
           break;
      case ENOENT:
	   ret = PAPI_ENOEVNT;
           break;
	case ESRCH:	/* If cannnot find process to attach to */
      case ENOSYS:
      case EAGAIN:
      case EBUSY:
      case E2BIG:	/* Only happens if attr is the wrong size somehow */
      case EBADF:	/* We are attempting to group with an invalid file descriptor */
	   ret = PAPI_ESYS;
	   break;
      case ENOMEM:
	   ret = PAPI_ENOMEM;
	   break;
      case EMFILE:	/* Out of file descriptors.  Typically max out at 1024 */
           ret = PAPI_ECOUNT;
           break;
      case EINVAL:
      default:
	   ret = PAPI_EINVAL;
           break;
   }
   return ret;
}

/* Open all events in the control state */
static int open_pe_events( carmel_context_t *ctx, carmel_control_state_t *ctl )
{

	int i, ret = PAPI_OK;
	long pid;
	/* Set the pid setting */
	/* If attached, this is the pid of process we are attached to. */
	/* If GRN_THRD then it is 0 meaning current process only */
	/* If GRN_SYS then it is -1 meaning all procs on this CPU */
	/* Note if GRN_SYS then CPU must be specified, not -1 */

	/** In the case of the uncore events we will not attach to a specific process so it will be -1*/
	if (ctl->attached) {
		pid = ctl->tid;
	}
	else {
		if (ctl->granularity==PAPI_GRN_SYS) {
			pid = -1;
		}
		else {
			pid = 0;
		}
	}
	/** The cpu is also a special case as we will specify it as 0 always for uncore events, the config field
	 * will contain the correct core unit, if necessary*/
	// long cpu = 0;
	for( i = 0; i < ctl->num_events; i++ ) {

		ctl->events[i].event_opened=0;

		/* set up the attr structure.			*/
		/* We don't set up all fields here		*/
		/* as some have already been set up previously.	*/

		/* group leader (event 0) is special                */
		/* If we're multiplexed, everyone is a group leader */
		if(ctl->events[i].unit == UNIT_CORE){
			// deal as core event
			if(ctl->group_leader_core == -1 || ctl->multiplexed){
				ctl->events[i].attr.pinned = !ctl->multiplexed;
				ctl->events[i].attr.disabled = 1;
				ctl->events[i].group_leader_fd=-1;
				ctl->events[i].attr.read_format = get_read_format(
					ctl->multiplexed,
					ctl->inherit,
					!ctl->multiplexed );
			}else {
				ctl->events[i].attr.pinned=0;
				ctl->events[i].attr.disabled = 0;
				ctl->events[i].group_leader_fd = ctl->group_leader_core;
				ctl->events[i].attr.read_format = get_read_format(
								ctl->multiplexed,
								ctl->inherit,
								0 );
			}

			/* try to open */
			ctl->events[i].event_fd = perf_event_open(
					&ctl->events[i].attr,
					pid,
					ctl->events[i].cpu,
					ctl->events[i].group_leader_fd,
					0x08); //
			SUBDBG("[CARMEL][DEBUG] open event #%d with fd %d, with: type=0x%3x, config=0x%3llx, leader=%d, cpu=%d, pid=%ld\n",
				i,
				ctl->events[i].event_fd,
				ctl->events[i].attr.type, 
				ctl->events[i].attr.config,
				ctl->events[i].group_leader_fd,
				ctl->events[i].cpu,
				pid
				);
			
			/* Try to match Linux errors to PAPI errors */
			if ( ctl->events[i].event_fd == -1 ) {
				
				SUBDBG("[CARMEL][ERROR]sys_perf_event_open event returned error "
					"on core event #%d.  Error: %s\n",
					i, strerror( errno ) );
				ret=map_perf_event_errors_to_papi(errno);

				/* We encountered an error, close up the fds we successfully opened.  */
				/* We go backward in an attempt to close group leaders last, although */
				/* That's probably not strictly necessary.                            */
				while ( i > 0 ) {
					i--;
					if (ctl->events[i].event_fd>=0) {
						close( ctl->events[i].event_fd );
						ctl->events[i].event_opened=0;
					}
				}
				return ret;
			}else{
				if(ctl->group_leader_core == -1 && !ctl->multiplexed){
					ctl->group_leader_core = ctl->events[i].event_fd;
				}
			}

		}else{
			//deal as uncore event
			if (ctl->group_leader_uncore == -1 || (ctl->multiplexed)) {
				ctl->events[i].attr.pinned = 0;//!ctl->multiplexed;
				ctl->events[i].attr.disabled = 1;
				ctl->events[i].group_leader_fd=-1;
				ctl->events[i].attr.read_format = get_read_format(
								ctl->multiplexed,
								ctl->inherit,
								!ctl->multiplexed );
			} else {
				ctl->events[i].attr.pinned=0;
				ctl->events[i].attr.disabled = 1;
				ctl->events[i].group_leader_fd= ctl->group_leader_uncore;
				ctl->events[i].attr.read_format = get_read_format(
								ctl->multiplexed,
								ctl->inherit,
								0 );
			}

			// ctl->events[i].attr.freq = 1;
			// ctl->events[i].attr.sample_freq = 4000; //default seems to be 4000 Hz

			SUBDBG("[CARMEL] trying to open uncore event with: type=0x%3x, config=0x%3x, leader=%d\n",
				ctl->events[i].attr.type, 
				ctl->events[i].attr.config,
				ctl->events[i].group_leader_fd
				);
			/* try to open */
			ctl->events[i].event_fd = perf_event_open(
					&ctl->events[i].attr,
					-1,
					0,
					ctl->events[i].group_leader_fd,
					0x08); //if not working,try with PERF_FORMAT_GROUP
			SUBDBG("[CARMEL][DEBUG] open uncore event #%d with fd %d, with: type=0x%3x, config=0x%3llx, leader=%d, cpu=%d, pid=%d\n",
				i,
				ctl->events[i].event_fd,
				ctl->events[i].attr.type, 
				ctl->events[i].attr.config,
				ctl->events[i].group_leader_fd,
				0,
				-1
				);	
			/* Try to match Linux errors to PAPI errors */
			if ( ctl->events[i].event_fd == -1 ) {
				
				SUBDBG("[CARMEL][ERROR]sys_perf_event_open returned error "
					"on uncore event #%d.  Error: %s\n",
					i, strerror( errno ) );
				ret=map_perf_event_errors_to_papi(errno);

				/* We encountered an error, close up the fds we successfully opened.  */
				/* We go backward in an attempt to close group leaders last, although */
				/* That's probably not strictly necessary.                            */
				while ( i > 0 ) {
					i--;
					if (ctl->events[i].event_fd>=0) {
						close( ctl->events[i].event_fd );
						ctl->events[i].event_opened=0;
					}
				}
				return ret;
			}else{
				if(ctl->group_leader_uncore == -1 && !ctl->multiplexed){
					ctl->group_leader_uncore = ctl->events[i].event_fd;
				}
			}
		}
		ctl->events[i].event_opened=1;
	}
	/* Set num_evts only if completely successful */
	ctx->state |= EVENTS_OPENED;
	return PAPI_OK;
}


/** Triggered by eventset operations like add or remove */

int _carmel_update_control_state( hwd_control_state_t *ctl, 
				    NativeInfo_t *native,
				    int count, 
				    hwd_context_t *ctx )
{
 	

   (void) ctx;
   int i;
   carmel_control_state_t *cu_ctl = (carmel_control_state_t *)ctl;
   carmel_context_t *cu_ctx = (carmel_context_t *) ctx;
   
   /* close all of the existing fds and start over again */
	/* In theory we could have finer-grained control and know if             */
	/* things were changed, but it's easier to tear things down and rebuild. */
   close_cu_events( cu_ctx, cu_ctl );

   SUBDBG("[CARMEL] Control state update! Will add %d events:\n",count);

   SUBDBG( "_carmel_update_control_state %p %p...", ctl, ctx );

   /* if no events, return */
   if (count==0) return PAPI_OK;
	int size_of_event_attr = sizeof(struct perf_event_attr);
	int core_unit =  (cu_ctl->cpu >-1 ?cu_ctl->cpu:sched_getcpu()) >> 1;
	int total_core = 0;
	int total_uncore = 0;
	/* set up all the events */
	for( i = 0; i < count; i++ ) {
		if ( native ) {
			int event_code = extract_event(native[i].ni_event);
			int cluster_setted = is_cluster_setted(native[i].ni_event);
			int cluster = extract_cluster(native[i].ni_event);
			
			SUBDBG("[CARMEL]  Seeing if event #%d exists with id 0x%x)\n", i, event_code);
			// printf("[CARMEL] Adding event with id 0x%x, papi code 0x%x\n",native[i].ni_event, native[i].ni_papi_code);
			carmel_native_event_entry_t entry;
			int event_index = carmel_event_index_of(event_code);
			if(event_index < 0){
				PAPIERROR("[CARMEL]  Event with code 0x%x not found in carmel component\n", event_code);
				return PAPI_ENOEVNT;
			}
			entry = carmel_native_table[event_index];
			// carmel_counter_t counter_info =  available_counters[event_code];
			SUBDBG("[CARMEL]  Event #%d is %s (from code 0x%x)\n", i, entry.name, event_code);
			//If L2 use core group (cpu/2), if SCF use 4
			unsigned int config;
			unsigned int type;
			if (entry.unit != UNIT_CORE)
			{
				/* assume uncore */
			 	config = entry.event;
				//  printf("[CARMEL] event_code input = %x\n", event_code);
				// printf("[CARMEL] event = %x\n", entry.event);
				// printf("[CARMEL] name = %s\n", entry.name);
				if(entry.unit == UNIT_L2) {
					if(cluster_setted == 1){
						config |= cluster;	
					}else{
						config |= core_unit;
					}
				}else{ //is UNIT_SCF
					config |= 1<<2;
				}
				// printf("[CARMEL] withcluster = %x\n", config);
				//  | (counter_info.unit == UNIT_L2? core_unit: 1<<2);
				type = PERF_TYPE_CARMEL;

				

				cu_ctl->event_uncore_position[total_uncore]=i;
				total_uncore++;
			}else{
				/* core event*/
				config = entry.event;
				type = PERF_TYPE_ARMV8;
				cu_ctl->event_core_position[total_core]=i;
				
				total_core++;
			}
			SUBDBG("[CARMEL]  config will be = 0x%x\n",  config);
			// printf("[CARMEL]  config will be = %x\n",  config);

			struct perf_event_attr pe;

			SUBDBG("[CARMEL]\t1. creating perf_event_attribute\n");
        	memset(&pe, 0, size_of_event_attr);
        	pe.type = type;
        	pe.size = size_of_event_attr;
        	pe.config = config;
        	pe.disabled = 1;
        	//pe.read_format = PERF_FORMAT_GROUP; // | PERF_FORMAT_ID;
			cu_ctl->events[i].attr = pe;
			cu_ctl->events[i].unit = entry.unit;
			
			SUBDBG("[CARMEL]\t2. copying perf_event_attribute to control\n");
			/* Move this events hardware config values and other attributes to the perf_events attribute structure */
			memcpy(&cu_ctl->events[i].attr, &pe, size_of_event_attr);

			// if cpu event mask not provided, then set the cpu to use to what may have been set on call to PAPI_set_opt (will still be -1 if not called)
			if (cu_ctl->cpu != -1) {
				cu_ctl->events[i].cpu = cu_ctl->cpu;
			}else{
				cu_ctl->events[i].cpu = sched_getcpu();
			}

			native[i].ni_position = i;
			
      } else {
    	  /* This case happens when called from _pe_set_overflow and _pe_ctl */
          /* Those callers put things directly into the cu_ctl structure so it is already set for the open call */
      }

      /* Copy the inherit flag into the attribute block that will be passed to the kernel */
      cu_ctl->events[i].attr.inherit = cu_ctl->inherit;

      /* Set the position in the native structure */
      /* We just set up events linearly           */
   }

	cu_ctl->num_events = count;
	cu_ctl->num_core_events = total_core;
	cu_ctl->num_uncore_events = total_uncore;


	/* actually open the events */
	SUBDBG("[CARMEL]Opening events!\n");

	int ret = open_pe_events( cu_ctx, cu_ctl );
	// SUBDBG("[CARMEL][DEBUG]EVENTS MAP:\n");
	// for(int i = 0; i < cu_ctl->num_core_events;i++){
	// 	SUBDBG("[CARMEL][DEBUG]Core Event %d is in position %d\n", i,cu_ctl->event_core_position[i]);
	// }

	// for(int i = 0; i < cu_ctl->num_uncore_events;i++){
	// 	SUBDBG("[CARMEL][DEBUG]Uncore Event %d is in position %d\n", i,cu_ctl->event_uncore_position[i]);
	// }
	SUBDBG("[CARMEL]\tresult of opening: %d\n",ret);
	if ( ret != PAPI_OK ) {
		SUBDBG("EXIT: open_pe_events returned: %d\n", ret);
      		/* Restore values ? */
		return ret;
	}

   return PAPI_OK;
}

/** Triggered by PAPI_reset() but only if the EventSet is currently running */
/*  If the eventset is not currently running, then the saved value in the   */
/*  EventSet is set to zero without calling this routine.                   */
int _carmel_reset( hwd_context_t *ctx, hwd_control_state_t *ctl )
{
	(void) ctx;
    carmel_control_state_t *cu_ctl = (carmel_control_state_t *)ctl;
  
	SUBDBG( "carmel_reset ctx=%p ctrl=%p...", ctx, ctl );

	/* Reset the hardware */
	// carmel_hardware_reset( event_ctx );

	/* We need to reset all of the events, not just the group leaders */
	for(int i = 0; i < cu_ctl->num_events; i++ ) {
		// if (  cu_ctl->events[i].group_leader_fd == -1 ) 
		{
			int ret = ioctl( cu_ctl->events[i].event_fd,
					PERF_EVENT_IOC_RESET, NULL );
			if ( ret == -1 ) {
				PAPIERROR("ioctl(%d, PERF_EVENT_IOC_RESET, NULL) "
						"returned error, Linux says: %s",
						cu_ctl->events[i].event_fd,
						strerror( errno ) );
				return PAPI_ESYS;
			}
		}
	}


	return PAPI_OK;
}

/** Triggered by PAPI_start() */
int _carmel_start( hwd_context_t *ctx, hwd_control_state_t *ctl )
{

	SUBDBG( "carmel_start %p %p...", ctx, ctl );
   	int ret = _carmel_reset( ctx, ctl );
	if ( ret ) {
		return ret;
	}
	int did_something = 0;
	carmel_control_state_t *cu_ctl = (carmel_control_state_t *)ctl;
   	carmel_context_t *cu_ctx = (carmel_context_t *) ctx;
	/* Enable all of the group leaders                */
	/* All group leaders have a group_leader_fd of -1 */
	for(int i = 0; i < cu_ctl->num_events; i++ ) {
		// if (cu_ctl->events[i].group_leader_fd == -1)
		 {
			SUBDBG("ioctl(enable): fd: %d\n",
				cu_ctl->events[i].event_fd);
			ret=ioctl( cu_ctl->events[i].event_fd,
				PERF_EVENT_IOC_ENABLE, NULL) ;

			/* ioctls always return -1 on failure */
			if (ret == -1) {
				PAPIERROR("ioctl(PERF_EVENT_IOC_ENABLE) failed");
				return PAPI_ESYS;
			}
			did_something++;
		}
	}

	if (!did_something) {
		PAPIERROR("Did not enable any counters");
		return PAPI_EBUG;
	}

	cu_ctx->state |= EVENTS_RUNNING;

	return PAPI_OK;
}


/** Triggered by PAPI_stop() */
int _carmel_stop( hwd_context_t *ctx, hwd_control_state_t *ctl )
{
	SUBDBG("[CARMEL] Stopping papi.\n");

	SUBDBG( "carmel_stop %p %p...", ctx, ctl );
	carmel_control_state_t *cu_ctl = (carmel_control_state_t *)ctl;
   	carmel_context_t *cu_ctx = (carmel_context_t *) ctx;
   
	SUBDBG("[CARMEL] Disabling %d events.\n",cu_ctl->num_events);

	/* Just disable the group leaders */
	for (int i = 0; i <  cu_ctl->num_events; i++ ) {
		// if (  cu_ctl->events[i].group_leader_fd == -1 ) 
		{
			int ret=ioctl(  cu_ctl->events[i].event_fd,
				PERF_EVENT_IOC_DISABLE, NULL);
			SUBDBG("[CARMEL] disable result for fd %d: %d.\n",cu_ctl->events[i].event_fd,ret);
			
			if ( ret == -1 ) {
				SUBDBG( "ioctl(%d, PERF_EVENT_IOC_DISABLE, NULL) "
					"returned error, Linux says: %s",
					 cu_ctl->events[i].event_fd, strerror( errno ) );
				return PAPI_EBUG;
			}
		}
	}

	cu_ctx->state &= ~EVENTS_RUNNING;
	return PAPI_OK;
}


/** Triggered by PAPI_read()     */
/*     flags field is never set? */
int _carmel_read( hwd_context_t *ctx, hwd_control_state_t *ctl,
			  long long **events, int flags )
{
   (void) ctx;
   (void) flags;
	carmel_control_state_t * cu_ctl = (carmel_control_state_t *) ctl;
   	int i, ret = -1;
	
	//Right now we are not leading correctly with multiplexing. What we assume is that we might have two leaders: core and uncore
	long long papi_core_buffer[BUFF_CORE_SIZE];
	long long papi_uncore_buffer[BUFF_UNCORE_SIZE];

	if(cu_ctl->num_core_events>0){
		if (cu_ctl->group_leader_core!=-1) {
			SUBDBG("[CARMEL][ERROR] Was expecting one group leader for core events");
		}
			ret = read(cu_ctl->group_leader_core,
					papi_core_buffer,
					sizeof ( papi_core_buffer ) );

		if ( ret == -1 ) {
			SUBDBG("[CARMEL][ERROR]core read returned an error: %s",
				strerror( errno ));
			return PAPI_ESYS;
		}

		if (ret<(signed)((1+cu_ctl->num_core_events)*sizeof(long long))) {
			SUBDBG("[CARMEL][ERROR] short read of core events, expected %d (x%d) but read %d (x%d)", 
			(signed)((1+cu_ctl->num_core_events)*sizeof(long long)), 1+cu_ctl->num_core_events,
			ret, ret/sizeof(long long));
			return PAPI_ESYS;
		}

		/* Make sure the kernel agrees with how many events we have */
		if (papi_core_buffer[0]!=cu_ctl->num_core_events) {
			SUBDBG("[CARMEL][ERROR] Wrong number of core events");
			return PAPI_ESYS;
		}
		/* put the count values in their proper location */
		for(i=0;i<cu_ctl->num_core_events;i++) {
			// SUBDBG("[CARMEL][DEBUG] Result of event #%d (core #%d)=%lld\n",cu_ctl->event_core_position[i],i,papi_core_buffer[1+i]);
			cu_ctl->counts[cu_ctl->event_core_position[i]] = papi_core_buffer[1+i];
		}
	}

	if(cu_ctl->num_uncore_events>0){
		if (cu_ctl->group_leader_uncore!=-1) {
			SUBDBG("[CARMEL][ERROR] Was expecting one group leader for uncore events");
		}
		ret = read(cu_ctl->group_leader_uncore,
				papi_uncore_buffer,
				sizeof ( papi_uncore_buffer ) );

		if ( ret == -1 ) {
			SUBDBG("[CARMEL][ERROR]uncore read returned an error: %s",
				strerror( errno ));
			// printf("[CARMEL][ERROR]uncore read returned an error: %s",strerror( errno ));
			return PAPI_ESYS;
		}

		if (ret<(signed)((1+cu_ctl->num_uncore_events)*sizeof(long long))) {
			SUBDBG("[CARMEL][ERROR] short read of uncore events, expected %d (x%d) but read %d (x%d)", 
			(signed)((1+cu_ctl->num_uncore_events)*sizeof(long long)), 1+cu_ctl->num_uncore_events,
			ret, ret/sizeof(long long));
			return PAPI_ESYS;
		}

		/* Make sure the kernel agrees with how many events we have */
		if (papi_uncore_buffer[0]!=cu_ctl->num_uncore_events) {
			SUBDBG("[CARMEL][ERROR] Wrong number of uncore events");
			return PAPI_ESYS;
		}

		/* put the count values in their proper location */
		for(i=0;i<cu_ctl->num_uncore_events;i++) {
			// SUBDBG("[CARMEL][DEBUG] Result of uncore event #%d (uncore #%d)=%lld\n",cu_ctl->event_uncore_position[i],i,papi_uncore_buffer[1+i]);
			cu_ctl->counts[cu_ctl->event_uncore_position[i]] = papi_uncore_buffer[1+i];
		}
	}

	/* point PAPI to the values we read */
	*events = cu_ctl->counts;
   return PAPI_OK;
}

/** Triggered by PAPI_write(), but only if the counters are running */
/*    otherwise, the updated state is written to ESI->hw_start      */
int _carmel_write( hwd_context_t *ctx, hwd_control_state_t *ctl,
			   long long *events )
{
	(void) ctx;
	(void) ctl;
	(void) events;
	return PAPI_ENOSUPP;
}


/** Triggered by PAPI_shutdown() */
int _carmel_shutdown_component(void)
{

	SUBDBG( "carmel_shutdown_component..." );

        /* Free anything we allocated */
   
	papi_free(carmel_native_table);

	return PAPI_OK;
}

/** Called at thread shutdown */
int _carmel_shutdown_thread( hwd_context_t *ctx )
{
	carmel_context_t *cu_ctx = ( carmel_context_t *) ctx;

	cu_ctx->initialized=0;

	return PAPI_OK;
}



/** This function sets various options in the component
  @param[in] ctx -- hardware context
  @param[in] code valid are PAPI_SET_DEFDOM, PAPI_SET_DOMAIN, 
                        PAPI_SETDEFGRN, PAPI_SET_GRANUL and PAPI_SET_INHERIT
  @param[in] option -- options to be set
 */
int _carmel_ctl( hwd_context_t *ctx, int code, _papi_int_option_t *option )
{
	SUBDBG( "carmel_ctl..." );
   	carmel_context_t *cu_ctx = (carmel_context_t *) ctx;
	carmel_control_state_t *cu_ctl = NULL;
   
   switch ( code ) {
      case PAPI_MULTIPLEX: //all multiplex stuff must be reviewed
	   cu_ctl = ( carmel_control_state_t * ) ( option->multiplex.ESI->ctl_state );
	   /* looks like we are allowed, so set multiplexed attribute */
	   cu_ctl->multiplexed = 1;
	   int ret = _carmel_update_control_state( cu_ctl, NULL,
						cu_ctl->num_events, cu_ctx );
	   if (ret != PAPI_OK) {
	      cu_ctl->multiplexed = 0;
	   }
	   return ret;

      case PAPI_ATTACH:
	   cu_ctl = ( carmel_control_state_t * ) ( option->attach.ESI->ctl_state );
	   cu_ctl->attached = 1;
	   cu_ctl->tid = option->attach.tid;
	//    printf("CPU NUM FOR THIS THREAD when attaching: %d\n",option->cpu.cpu_num);

	   /* If events have been already been added, something may */
	   /* have been done to the kernel, so update */
	   ret =_carmel_update_control_state( cu_ctl, NULL,
						cu_ctl->num_events, cu_ctx);
	   return ret;

      case PAPI_DETACH:
	   cu_ctl = ( carmel_control_state_t *) ( option->attach.ESI->ctl_state );
	   cu_ctl->attached = 0;
	   cu_ctl->tid = 0;
	   return PAPI_OK;

      case PAPI_CPU_ATTACH:
	   cu_ctl = ( carmel_control_state_t *) ( option->cpu.ESI->ctl_state );
	   cu_ctl->cpu = option->cpu.cpu_num;
	//    printf("SETTED CPU NUM: %d\n",cu_ctl->cpu);
	   return PAPI_OK;

      case PAPI_DOMAIN:
	   cu_ctl = ( carmel_control_state_t *) ( option->domain.ESI->ctl_state );
	   cu_ctl->domain = option->domain.domain;
	   return PAPI_OK;

      case PAPI_GRANUL:
		cu_ctl->granularity=PAPI_GRN_SYS;
		cu_ctl->cpu=sched_getcpu();
	   return PAPI_ENOSUPP;
      case PAPI_DEF_ITIMER:
	   return PAPI_ENOSUPP;
      case PAPI_DEF_ITIMER_NS:
	   return PAPI_ENOSUPP;
	  case PAPI_INHERIT:
	   cu_ctl = ( carmel_control_state_t *) ( option->domain.ESI->ctl_state );
	   /* looks like we are allowed, so set the requested inheritance */
	   if (option->inherit.inherit) {
	      /* children will inherit counters */
	      cu_ctl->inherit = 1;
	   } else {
	      /* children won't inherit counters */
	      cu_ctl->inherit = 0;
	   }
	   return PAPI_OK;
 	  case PAPI_DATA_ADDRESS:
	   return PAPI_ENOSUPP;
      case PAPI_INSTR_ADDRESS:
	   return PAPI_ENOSUPP;
      case PAPI_DEF_MPX_NS:
	   return PAPI_ENOSUPP;
      default:
	   return PAPI_ENOSUPP;
   	}
}

/** This function has to set the bits needed to count different domains
    In particular: PAPI_DOM_USER, PAPI_DOM_KERNEL PAPI_DOM_OTHER
    By default return PAPI_EINVAL if none of those are specified
    and PAPI_OK with success
    PAPI_DOM_USER is only user context is counted
    PAPI_DOM_KERNEL is only the Kernel/OS context is counted
    PAPI_DOM_OTHER  is Exception/transient mode (like user TLB misses)
    PAPI_DOM_ALL   is all of the domains
 */
int _carmel_set_domain( hwd_control_state_t * ctl, int domain )
{
    carmel_control_state_t *cu_ctl = ( carmel_control_state_t *) ctl;

	SUBDBG("old control domain %d, new domain %d\n", cu_ctl->domain,domain);
	cu_ctl->domain = domain;

	return PAPI_OK;
}


/**************************************************************/
/* Naming functions, used to translate event numbers to names */
/**************************************************************/


/** Enumerate Native Events
 *   @param EventCode is the event of interest
 *   @param modifier is one of PAPI_ENUM_FIRST, PAPI_ENUM_EVENTS
 *  If your component has attribute masks then these need to
 *   be handled here as well.
 */
int _carmel_ntv_enum_events( unsigned int *EventCode, int modifier )
{
  int index;


  switch ( modifier ) {

		/* return EventCode of first event */
	case PAPI_ENUM_FIRST:
	   /* return the first event that we support */

	   *EventCode = FIRST_EVENT_VALUE;

	   return PAPI_OK;

		/* return EventCode of next available event */
	case PAPI_ENUM_EVENTS:
	   index = *EventCode;
		// printf("NEXT EVENT: %d\n",index+1);

	   /* Make sure we have at least 1 more event after us */
	   if ( index <= MAX_EVENT_VALUE-1) {

	      /* This assumes a non-sparse mapping of the events */
	      *EventCode = *EventCode + 1;
	      return PAPI_OK;
	   } else {
	      return PAPI_ENOEVNT;
	   }
	   break;
	
	default:
	   return PAPI_EINVAL;
  }

  return PAPI_EINVAL;
}

/** Takes a native event code and passes back the name 
 * @param EventCode is the native event code
 * @param name is a pointer for the name to be copied to
 * @param len is the size of the name string
 */
int _carmel_ntv_code_to_name( unsigned int EventCode, char *name, int len )
{
	int event = extract_event(EventCode);
	for (int i = 0; i < CARMEL_MAX_TOTAL_COUNTERS; i++ )
		if ( event == carmel_native_table[i].event ) {
				// event = carmel_native_table[i].event;
				strncpy(name,carmel_native_table[i].name, len);
				return PAPI_OK;
		}
//   int index;

//   index = EventCode;

//   /* Make sure we are in range */
//   if (index >= 0 && index < CARMEL_MAX_TOTAL_COUNTERS) {
//      strncpy( name, carmel_native_table[index].name, len );  
//      return PAPI_OK;
//   }
   
  return PAPI_ENOEVNT;
}

/** Takes a native event code and passes back the event description
 * @param EventCode is the native event code
 * @param descr is a pointer for the description to be copied to
 * @param len is the size of the descr string
 */
int _carmel_ntv_code_to_descr( unsigned int EventCode, char *descr, int len )
{
	unsigned int event = extract_event(EventCode);
	for (int i = 0; i < CARMEL_MAX_TOTAL_COUNTERS; i++ )
		if ( event == carmel_native_table[i].event ) {
				// event = carmel_native_table[i].event;
				strncpy(descr,carmel_native_table[i].description, len);
				return PAPI_OK;
		}
//   int index;
//   index = EventCode;

//   /* make sure event is in range */
//   if (index >= 0 && index < CARMEL_MAX_TOTAL_COUNTERS) {
//      strncpy( descr, carmel_native_table[index].description, len );
//      return PAPI_OK;
//   }
  
  return PAPI_ENOEVNT;
}



int _carmel_ntv_name_to_code( const char *name, unsigned int *evtcode ) {

	char *pcolon;
	// char event_name[mask_start+1]; 
	char *event_name;
	event_name = strdup(name);
	pcolon = strchr( name, ':' );
	// unsigned int event = 0;
	unsigned int cluster = 0;
	unsigned int cluster_setted = 0;
	// Found colon separator
	if ( pcolon != NULL ) {
		int mask_start = pcolon - name;
		int mask_size = strlen(name)-mask_start-1;
		// char event_name[mask_start+1];
		char mask[mask_size+1];
		strncpy( event_name, name,  mask_start);
		event_name[mask_start] = '\0';
		strncpy( mask, pcolon+1, mask_size );
		mask[mask_size] = '\0';
		
		// printf("mask: %s(size=%ld), event_name: %s (size=%d)\n",mask,mask_size,event_name,mask_start);
		if(strncmp(CLUSTER_ATTRIBUTE,mask,CLUSTER_ATTRIBUTE_SIZE) == 0){
			char *eq;
			eq = strchr( mask, '=' );
			if ( eq != NULL ) {
				int value_start = eq - mask;
				int value_size = strlen(mask)-value_start-1;
				char value[value_size+1];
				strncpy( value, eq+1, value_size );
				value[value_size] = '\0';
				// printf("THIS IS THE VALUE FOR THE CLUSTER: %s\n",value);
				int ivalue = atoi(value);
				if(ivalue < 0 || ivalue >3){
					PAPIERROR( "[CARMEL] attribute "CLUSTER_ATTRIBUTE" has to be setted with a value between 0-3, but was setted with %s", value);
					return PAPI_EATTR;
				}
				cluster = ivalue;
				cluster_setted = 1;
			}
			else{
				PAPIERROR( "[CARMEL] attribute "CLUSTER_ATTRIBUTE" has to be setted with a value between 0-3, but was setted with %s", "none");
				return PAPI_EATTR;
			}
		}else{
			PAPIERROR( "[CARMEL] attribute %s for event %s is not valid", event_name, mask );
			return PAPI_EATTR;
		}
	}

	// printf("SEEKING EVENT WITH NAME %s\n",event_name);
	carmel_native_event_entry_t entry;
	int i;
	for (i = 0; i < CARMEL_MAX_TOTAL_COUNTERS; i++ )
		if ( strcmp( event_name, carmel_native_table[i].name ) == 0 ) {
				// event = carmel_native_table[i].event;
				entry = carmel_native_table[i];
				break;
		}
	
	if (i >= CARMEL_MAX_TOTAL_COUNTERS)
		return PAPI_ENOEVNT;
	
	if (cluster_setted && entry.unit != UNIT_L2){
		PAPIERROR( "[CARMEL] attribute "CLUSTER_ATTRIBUTE" for event %s is not valid", event_name);
		return PAPI_EATTR;
	}

	unsigned int tmp = entry.event << NATIVE_EVENT_SHIFT;
	if(cluster_setted){
		tmp = tmp | (cluster << CLUSTER_VALUE_SHIFT);
		tmp = tmp | (1 << CLUSTER_SETTED_SHIFT);
	}
	*evtcode = (int) (tmp | PAPI_NATIVE_MASK);
	// printf("EVT CODE: 0x%x\n",*evtcode);
	return PAPI_OK;
}



