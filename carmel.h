/*
*  Copyright 2022 Instituto Superior de Engenharia do Porto
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <errno.h>
#include <string.h>


/* Headers required by PAPI */
#include "papi.h"
#include "papi_internal.h"
#include "papi_vector.h"
#include "papi_memory.h"    /* defines papi_malloc(), etc. */
#include <sched.h>
#include "carmel_events.h"

/* Maximum size we ever expect to read from a perf_event fd   */
/*  (this is the number of 64-bit values)                     */
/* We use this to size the read buffers                       */
/* The three is for event count, time_enabled, time_running   */
/*  and the counter term is count value and count id for each */
/*  possible counter value.                                   */
#define BUFF_CORE_SIZE (3 + (2 * CARMEL_TOTAL_CORE_COUNTERS))
#define BUFF_UNCORE_SIZE (3 + (2 * CARMEL_TOTAL_UNCORE_COUNTERS))



/* Defines for ctx->state */
#define EVENTS_OPENED  0x01
#define EVENTS_RUNNING 0x02

// The following macro follows if a string function has an error. It should 
// never happen; but it is necessary to prevent compiler warnings. We print 
// something just in case there is programmer error in invoking the function.
#define HANDLE_STRING_ERROR {fprintf(stderr,"%s:%i unexpected string function error.\n",__FILE__,__LINE__); exit(-1);}



#define NATIVE_EVENT_SHIFT 0
#define CLUSTER_VALUE_SHIFT 12
#define CLUSTER_SETTED_SHIFT 16

#define EVENT_MASK 0xFFF
#define CLUSTER_MASK 0xF000
#define CLUSTER_SETTED_MASK 0xF0000

typedef struct
{
  int group_leader_fd;            /* fd of group leader                   */
  int event_fd;                   /* fd of event                          */
  int event_opened;               /* event successfully opened            */
  int profiling;                  /* event is profiling                   */
  int sampling;			  /* event is a sampling event            */
  uint32_t nr_mmap_pages;         /* number pages in the mmap buffer      */
  void *mmap_buf;                 /* used for control/profiling           */
  uint64_t tail;                  /* current read location in mmap buffer */
  uint64_t mask;                  /* mask used for wrapping the pages     */
  int cpu;                        /* cpu associated with this event       */
  int unit; //UNIT_CORE, UNIT_L2 or UNIT_SCF
  struct perf_event_attr attr;    /* perf_event config structure          */
} carmel_event_info_t;

/** Holds control flags.  
 *    There's one of these per event-set.
 *    Use this to hold data specific to the EventSet, either hardware
 *      counter settings or things like counter start values 
 */
typedef struct carmel_control_state
{
  int num_events;
  int num_core_events;
  int num_uncore_events;
  int group_leader_core;
  int group_leader_uncore;
  int domain;
  int multiplexed;
  int granularity;
  int overflow;
  int inherit;
  int attached;
  int cpu;                        /* which cpu to measure (used to calculate group unit) */
  pid_t tid;                      /* thread we are monitoring (might not be used) */
  carmel_event_info_t events[CARMEL_MAX_TOTAL_COUNTERS];
  long long counts[CARMEL_MAX_TOTAL_COUNTERS];
  int event_core_position[CARMEL_TOTAL_CORE_COUNTERS];
  int event_uncore_position[CARMEL_TOTAL_UNCORE_COUNTERS];
} carmel_control_state_t;

/** Structure that stores private information for each event */
typedef struct carmel_register
{
   unsigned int selector;
		           /**< Signifies which counter slot is being used */
			   /**< Indexed from 1 as 0 has a special meaning  */
} carmel_register_t;

/** This structure is used to build the table of events  */
/*   The contents of this structure will vary based on   */
/*   your component, however having name and description */
/*   fields are probably useful.                         */
typedef struct carmel_native_event_entry
{
	carmel_register_t resources;	    /**< Per counter resources       */
	char name[PAPI_MAX_STR_LEN];	    /**< Name of the counter         */
	char description[PAPI_MAX_STR_LEN]; /**< Description of the counter  */
	unsigned int event;
	unsigned int unit;
	int writable;			    /**< Whether counter is writable */
	/* any other counter parameters go here */
} carmel_native_event_entry_t;

/** This structure is used when doing register allocation 
    it possibly is not necessary when there are no 
    register constraints */
typedef struct carmel_reg_alloc
{
	carmel_register_t ra_bits;
} carmel_reg_alloc_t;



/** Holds per-thread information */
typedef struct carmel_context
{
	int initialized;                /* are we initialized?           */
	int state;                      /* are we opened and/or running? */
} carmel_context_t;

int _carmel_init_component( int cidx );
int _carmel_init_thread( hwd_context_t *ctx );
int _carmel_init_control_state( hwd_control_state_t * ctl );
int _carmel_update_control_state( hwd_control_state_t *ctl, NativeInfo_t *native, int count, hwd_context_t *ctx );
int _carmel_reset( hwd_context_t *ctx, hwd_control_state_t *ctl );
int _carmel_start( hwd_context_t *ctx, hwd_control_state_t *ctl );
int _carmel_stop( hwd_context_t *ctx, hwd_control_state_t *ctl );
int _carmel_read( hwd_context_t *ctx, hwd_control_state_t *ctl,long long **events, int flags );
int _carmel_write( hwd_context_t *ctx, hwd_control_state_t *ctl, long long *events );
int _carmel_shutdown_component(void);
int _carmel_shutdown_thread( hwd_context_t *ctx );
int _carmel_ctl( hwd_context_t *ctx, int code, _papi_int_option_t *option );
int _carmel_set_domain( hwd_control_state_t * ctl, int domain );
int _carmel_ntv_enum_events( unsigned int *EventCode, int modifier );
int _carmel_ntv_code_to_name( unsigned int EventCode, char *name, int len );
int _carmel_ntv_code_to_descr( unsigned int EventCode, char *descr, int len );
int _carmel_ntv_name_to_code( const char *name, unsigned int *evtcode );

/** Vector that points to entry points for our component */
papi_vector_t _carmel_vector = {
	.cmp_info = {
		/* default component information */
		/* (unspecified values are initialized to 0) */
                /* we explicitly set them to zero in this example */
                /* to show what settings are available            */

		.name = "carmel",
		.short_name = "carmel",
		.description = "Xavier's Carmel core and uncore counters.",
		.version = "0.4",
		.support_version = "n/a",
		.kernel_version = "n/a",
		.num_cntrs =               CARMEL_MAX_TOTAL_COUNTERS, //FIX-ME
		.num_mpx_cntrs =           CARMEL_MAX_TOTAL_COUNTERS, //FIX-ME
		.default_domain =          PAPI_DOM_USER,
		.available_domains =       PAPI_DOM_USER,
		.default_granularity =     PAPI_GRN_THR,
		.available_granularities = PAPI_GRN_THR,
		.hardware_intr_sig =       PAPI_INT_SIGNAL,

		/* component specific cmp_info initializations */
	},

	/* sizes of framework-opaque component-private structures */
	.size = {
	        /* once per thread */
		.context = sizeof ( carmel_context_t ),
	        /* once per eventset */
		.control_state = sizeof ( carmel_control_state_t ),
	        /* ?? */
		.reg_value = sizeof ( carmel_register_t ),
	        /* ?? */
		.reg_alloc = sizeof ( carmel_reg_alloc_t ),
	},

	/* function pointers */
        /* by default they are set to NULL */
   
	/* Used for general PAPI interactions */
	.start =                _carmel_start,
	.stop =                 _carmel_stop,
	.read =                 _carmel_read,
	.reset =                _carmel_reset,	
	.write =                _carmel_write,
	.init_component =       _carmel_init_component,	
	.init_thread =          _carmel_init_thread,
	.init_control_state =   _carmel_init_control_state,
	.update_control_state = _carmel_update_control_state,	
	.ctl =                  _carmel_ctl,	
	.shutdown_thread =      _carmel_shutdown_thread,
	.shutdown_component =   _carmel_shutdown_component,
	.set_domain =           _carmel_set_domain,
	/* .cleanup_eventset =     NULL, */
	/* called in add_native_events() */
	/* .allocate_registers =   NULL, */

	/* Used for overflow/profiling */
	/* .dispatch_timer =       NULL, */
	/* .get_overflow_address = NULL, */
	/* .stop_profiling =       NULL, */
	/* .set_overflow =         NULL, */
	/* .set_profile =          NULL, */

	/* ??? */
	/* .user =                 NULL, */

	/* Name Mapping Functions */
	.ntv_enum_events =   _carmel_ntv_enum_events,
	.ntv_code_to_name =  _carmel_ntv_code_to_name,
	.ntv_code_to_descr = _carmel_ntv_code_to_descr,
        /* if .ntv_name_to_code not available, PAPI emulates  */
        /* it by enumerating all events and looking manually  */
   	.ntv_name_to_code  = _carmel_ntv_name_to_code,

   
	/* These are only used by _papi_hwi_get_native_event_info() */
	/* Which currently only uses the info for printing native   */
	/* event info, not for any sort of internal use.            */
	/* .ntv_code_to_bits =  NULL, */

};